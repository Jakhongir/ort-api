package uz.ort.api.constants;

public enum VerticleUIDs {
    DEMO_DATABASE_VERTICLE("DemoDatabaseVerticle_UID"),
    CABINET_API_DATABASE_VERTICLE("CabinetApiDatabaseVerticle_UID"),
    NO_VERTICLE("NoVerticle");

    private String val;

    VerticleUIDs(String val) {
        this.val = val;
    }

    public String val() {
        return val;
    }
}
