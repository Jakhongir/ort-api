package uz.ort.api.routers;

import io.vertx.core.eventbus.DeliveryOptions;
import io.vertx.ext.web.Router;

public interface RoutingService {
    Router getRouter();

    static DeliveryOptions getDeliveryOptions(String action) {
        return (new DeliveryOptions()).addHeader("action", action);
    }
}
