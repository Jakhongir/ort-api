package uz.ort.api.verticles;

import io.vertx.core.AbstractVerticle;
import io.vertx.core.Future;
import io.vertx.core.eventbus.Message;
import io.vertx.core.json.JsonArray;
import io.vertx.core.json.JsonObject;
import io.vertx.ext.jdbc.JDBCClient;
import io.vertx.ext.sql.SQLConnection;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import uz.ort.api.constants.ErrorCodes;
import uz.ort.api.constants.VerticleUIDs;
import uz.ort.api.database.QueryDemo;
import uz.ort.api.utils.ConfigService;

import static uz.ort.api.utils.ResponseUtil.reportQueryError;

public class DemoDatabaseVerticle extends AbstractVerticle {

    private static final Logger LOGGER = LoggerFactory.getLogger("DemoDatabaseVerticle");

    private JDBCClient dbClient;
    private static final String UID = VerticleUIDs.DEMO_DATABASE_VERTICLE.val();
    private QueryDemo queryDemo = new QueryDemo();

    @Override
    public void start(Future<Void> startFuture) throws Exception {
        LOGGER.info("Starting DemoDatabaseVerticle ...");
        this.dbClient = JDBCClient.createShared(
                vertx,
                ConfigService.getPostgreConf(config().getJsonObject("db")),
                config().getJsonObject("db").getString("datasource", "datasourcedbOrt"));
        dbClient.getConnection(ar -> {
            if (ar.failed()) {
                LOGGER.error("Could not open a database connection", ar.cause());
                startFuture.fail(ar.cause());
            } else {
                SQLConnection connection = ar.result();
                LOGGER.info("Connection :" + ar.succeeded());
                vertx.eventBus().consumer(UID, this::onMessage);
                startFuture.complete();
            }
        });
    }

    public void onMessage(Message<JsonObject> message) {

        if (!message.headers().contains("action")) {
            LOGGER.error("No action header specified for message with headers " +
                    message.headers() + " and body {" + message.body().encodePrettily() + "}");
            message.fail(ErrorCodes.NO_ACTION_SPECIFIED.val(), "No action header specified");
            return;
        }
        String action = message.headers().get("action");

        switch (action) {
            case "getList":
                getList(message);
                break;
            default:
                message.fail(ErrorCodes.BAD_ACTION.ordinal(), "Bad action: " + action);
        }
    }


    private void getList(Message<JsonObject> message) {
        String authCode = "";
        if (message.body().containsKey("authCode")) {
            authCode = message.body().getString("authCode");
        }
        JsonArray params = new JsonArray().add(authCode);

        dbClient.query(
                queryDemo.GET_LIST,
//                params,
                res -> {
                    if (res.succeeded()
                            && res.result() != null) {
                        message.reply(new JsonObject().put("list", res.result().getRows()));
                    } else {
                        reportQueryError(message, res.cause());
                    }
                });
    }
}
