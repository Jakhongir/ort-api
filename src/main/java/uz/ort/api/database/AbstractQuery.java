package uz.ort.api.database;

public class AbstractQuery {
    public String GET_LIST;
    public String GET_BY_ID;
    public String INSERT;
    public String UPDATE;
    public String DELETE;
}
